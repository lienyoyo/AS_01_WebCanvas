# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here


## Canvas
<img src="/assets/readme/canvas.png"></img>
* 畫布主體



## Menu


### Color Selector
<img src="/assets/readme/color selector.png"></img>

* 可於上面的圖中選取顏色
* 右下顯示的為現在使用的顏色
* 左下顯示的為鼠標目前在上圖中指到的顏色




### Tools
<img src="/assets/readme/btns.png"></img>

1. Brush: 可在畫布上畫出線條，其粗細可由 Width Bar 調整。
    * 點選後直接在畫布上點擊並移動滑鼠即可。
2. Eraser: 可擦去畫布上的東西，其粗細可由 Width Bar 調整。
    * 點選後直接在畫布上點擊並移動滑鼠即可。
3. Text: 可在畫布上寫上文字，在 Stroke 模式時，其粗細可由 Width Bar 調整。
    * 點選後在畫布上點擊，會出現一文字輸入框，待輸入完成後，點擊畫布其餘區域即可將文字畫上畫布。
4. Circle: 可在畫布上畫出圓形，在 Stroke 模式時，其粗細可由 Width Bar 調整。
    * 點選後在畫布上點擊並移動滑鼠即可。點擊處為圓心，移動滑鼠可改變其大小。
    * 在放開滑鼠之前皆可預覽會畫出的效果。
5. Rectangle: 可在畫布上畫出矩形，在 Stroke 模式時，其粗細可由 Width Bar 調整。
    * 點選後在畫布上點擊並移動滑鼠即可。移動滑鼠可改變其大小。
    * 在放開滑鼠之前皆可預覽會畫出的效果。
6. Triangle: 可在畫布上畫出三角形，在 Stroke 模式時，其粗細可由 Width Bar 調整。
    * 點選後在畫布上點擊並移動滑鼠即可。移動滑鼠可改變其大小。
    * 在放開滑鼠之前皆可預覽會畫出的效果。
7. Undo: 可還原至上一動作。
    * 直接點選即可。
8. Redo: 可重作下一動作("還原"還原)。
    * 直接點選即可。
9. ReFresh: 可清空畫布。
    * 直接點選即可。
10. Line: 可在畫布上畫出直線，其粗細可由 Width Bar 調整。
    * 點選後在畫布上點擊並移動滑鼠即可。會從點擊處畫至鼠標處。
    * 在放開滑鼠之前皆可預覽會畫出的效果。
11. Range Eraser: 範圍版 Eraser，可擦去畫布上的東西。
    * 點選後在畫布上點擊並移動滑鼠即可。會顯示矩形範圍，範圍內會被擦去。
    * 在放開滑鼠之前皆可預覽矩形範圍。

<img src="/assets/readme/image upload.png"></img>

12. Paste Image: 可在畫布上貼上上傳的圖片。
    * 點選右邊的"選擇檔案"來選擇圖片，再點選左邊的 Paste Image 來選取此功能。(若無先選取圖片或上傳不符格式的圖片則會有警告)
    * 接著在畫布上點擊即可，點擊處將會是該圖片的左上角。




### Tools Config
<img src="/assets/readme/width.png"></img>

1. Stroke: 可轉換成 Stroke 模式(預設)。
    * 點選後，畫出的圖形皆會是空心、只有外框的樣式。
2. Fill: 可轉換成 Fill 模式。
    * 點選後，畫出的圖形皆會是填滿的樣式。
3. Width Bar: 可調整 Brush、Eraser、Stroke 模式圖形的粗細。
    * 拉動右邊的紅點即可調整，左邊最細，右邊最粗。

<img src="/assets/readme/font style.png"></img>

4. Font Size Bar: 可調整文字的大小。
    * 拉動右邊的紅點即可調整，左邊最小，右邊最大。

5. Font Select List: 可調整文字的字型。(預設: Montserrat)
    * 點選下拉選單即可選擇。

<img src="/assets/readme/fonts.png"></img>




## Download your Masterpiece
<img src="/assets/readme/download.png"></img>

* 點選即可將畫布的內容下載成圖片。


## Cursor Style
<img src="/assets/readme/cursor normal.png"></img>

* 一般的鼠標樣式。

<img src="/assets/readme/cursor pen.png"></img>

* 選取 Brush 後，鼠標在畫布上的樣式。

<img src="/assets/readme/cursor eraser.png"></img>

* 選取 Eraser 後，鼠標在畫布上的樣式。

<img src="/assets/readme/cursor text.png"></img>

* 選取 Text 後，鼠標在畫布上的樣式。

<img src="/assets/readme/cursor pre.png"></img>

* 選取 Circle、Rectangle、Triangle、Line、Range Eraser 後，鼠標在畫布上的樣式。