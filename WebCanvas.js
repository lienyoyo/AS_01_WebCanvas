var myCanvas;
var myContext;

var myCanvas_buffer;
var myContext_buffer;

var myColorSelec_Canvas;
var myColorSelec;
var myShowColor;
var myShowColor_now;

var color_now_R = 0;
var color_now_G = 0;
var color_now_B = 0;

var IsTextExist = false;
var text_x;
var text_y;

var text_size = 30;
var text_font = "'Montserrat', sans-serif";

var drawtype = 0;
// stroke = 0;
// fill = 1;


var mode = 0;
// brush = 0;
// eraser = 1;
// text = 2;
// circle = 3;
// rectangle = 4;
// triangle = 5;
// image = 6;
// line = 7;



var shapefirstpoint_x;
var shapefirstpoint_y;

var imgupload;

var myhistory = {
    undo_num: 0,
    redo_num: 0,
    redo_list: [],
    undo_list: []
};

function Loading()
{
    myCanvas = document.getElementById('myCanvas');
    myContext = myCanvas.getContext('2d');
    myContext.globalCompositeOperation = "source-over";

    myContext.font = text_size + "px 'Montserrat', sans-serif";
    // myContext.font = text_size + "px Arial";
    myContext.textBaseline = "top";


    myCanvas.addEventListener('mousedown', myCanvas_mousedown);

    myCanvas.addEventListener('mouseup', function(){
        myCanvas.removeEventListener('mousemove', mouseMove, false);
        console.log("Mouse up");
    }, false);

/************************************************************ */

    myCanvas_buffer = document.getElementById('myCanvas_buffer');
    myContext_buffer = myCanvas_buffer.getContext('2d');
    myContext_buffer.globalCompositeOperation = "source-over";

    myCanvas_buffer.addEventListener('mousedown', myCanvasBuffer_mousedown);

    myCanvas_buffer.addEventListener('mouseup', myCanvasBuffer_mouseup, false);




    createColorSelector();

    console.log("loaded");
}




function createColorSelector()
{
    myColorSelec_Canvas = document.getElementById('myColorS');
    myColorSelec = myColorSelec_Canvas.getContext('2d');
    myShowColor = document.getElementById('myShowColor');
    myShowColor_now = document.getElementById('myShowColor_now');

    myColorSelec_Canvas.addEventListener('mousedown', pick);

    for (var i=0; i<360; i++)
    {
        for (var j=0; j<100; j++)
        {
            myColorSelec.fillStyle = "hsl(" + i + "," + j + "%,50%)";
            myColorSelec.fillRect(i,99-j,1,1);
        }
    }

    myColorSelec.fillStyle = "rgb(0,0,0)";
    myColorSelec.fillRect(0,100,179,150);
    myColorSelec.fillStyle = "rgb(255,255,255)";
    myColorSelec.fillRect(179,100,360,150);

    myColorSelec_Canvas.addEventListener('mousemove', picking);

    console.log("ColorSelector : created");
}






function onclick_typestroke()
{
    drawtype = 0;
    console.log("Change to stroke");
}

function onclick_typefill()
{
    drawtype = 1;
    console.log("Change to fill");
}

function onclick_brush()
{
    mode = 0;
    myCanvas_buffer.style.visibility = "hidden";
    if(IsTextExist)
    {
        drawText();
    }
    myContext.globalCompositeOperation = "source-over";
    myContext.strokeStyle = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';

    myCanvas.style.cursor = "url(./assets/SimBlack/Handwriting.cur), auto";
}

function onclick_eraser()
{
    mode = 1;
    myCanvas_buffer.style.visibility = "hidden";
    if(IsTextExist)
    {
        drawText();
    }
    myContext.globalCompositeOperation = "destination-out";
    myContext.strokeStyle = "rgb(0,0,0)";

    myCanvas.style.cursor = "url(./assets/SimBlack/Move.cur), auto";
}

function onclick_text()
{
    mode = 2;
    myCanvas_buffer.style.visibility = "hidden";
    myContext.globalCompositeOperation = "source-over";
    myContext.strokeStyle = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';
    myContext.fillStyle = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';
    // myCanvas_buffer.style.visibility="visible";
    myCanvas.style.cursor = "url(./assets/SimBlack/Text.cur), text";
}

function onclick_refresh()
{
    if(IsTextExist)
    {
        document.getElementById('div_canvas').removeChild(document.getElementById('myTextinput'));
        IsTextExist = false;
    }
    myContext.clearRect(0,0,800,600);
    console.log("refresh");
}

function onclick_circle()
{
    mode = 3;
    if(IsTextExist)
    {
        drawText();
    }
    myContext.globalCompositeOperation = "source-over";
    myCanvas_buffer.style.visibility = "visible";
    myCanvas_buffer.style.cursor = "url(./assets/SimBlack/Precision.cur), crosshair";
    myContext.strokeStyle = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';
    myContext.fillStyle = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';
    console.log("circle");
}

function onclick_rectangle()
{
    mode = 4;
    if(IsTextExist)
    {
        drawText();
    }
    myContext.globalCompositeOperation = "source-over";
    myCanvas_buffer.style.visibility = "visible";
    myCanvas_buffer.style.cursor = "url(./assets/SimBlack/Precision.cur), crosshair";
    myContext.strokeStyle = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';
    myContext.fillStyle = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';
    console.log("rectangle");
}

function onclick_triangle()
{
    mode = 5;
    if(IsTextExist)
    {
        drawText();
    }
    myContext.globalCompositeOperation = "source-over";
    myCanvas_buffer.style.visibility = "visible";
    myCanvas_buffer.style.cursor = "url(./assets/SimBlack/Precision.cur), crosshair";
    myContext.strokeStyle = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';
    myContext.fillStyle = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';
    console.log("triangle");
}

function imageupload(fileDom)
{
    mode = 6;
    if(IsTextExist)
    {
        drawText();
    }
    myContext.globalCompositeOperation = "source-over";
    myCanvas_buffer.style.visibility = "hidden";
    myCanvas.style.cursor = "url(./assets/SimBlack/Precision.cur), crosshair";
    myContext.strokeStyle = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';
    myContext.fillStyle = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';


    //判斷是否支持FileReader
    if (window.FileReader) {
        var reader = new FileReader();
    } else {
        alert("您的設備不支持圖片預覽功能，如需該功能請升級您的設備！");
    }
    //獲取文件信息
    var file = fileDom.files[0];

    if(fileDom.files[0] == null)
    {
        imgupload = null;
        return;
    }

    var imageType = /^image\//;
    //是否是圖片
    if (!imageType.test(file.type)) {
        alert("請選擇正確的圖片！");
        imgupload = null;
    }else {
    //讀取完成
            reader.onload = function (e) {
                imgupload = new Image();
                imgupload.src = e.target.result;
            };
            reader.readAsDataURL(file);
    }
}

function onclick_imagepaste()
{
    mode = 6;
    if(IsTextExist)
    {
        drawText();
    }
    myContext.globalCompositeOperation = "source-over";
    myCanvas_buffer.style.visibility = "hidden";
    myCanvas.style.cursor = "url(./assets/SimBlack/Precision.cur), crosshair";
    myContext.strokeStyle = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';
    myContext.fillStyle = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';
}

function undo()
{
    if(myhistory.undo_num == 0)
    {
        console.log("no undo");
        return;
    }
    else
    {
        myhistory.undo_num--;
        myhistory.redo_num++;

        var tmp = myhistory.undo_list.pop();
        var now = myCanvas.toDataURL();
        myhistory.redo_list.push(now);

        var tmp_image = new Image();
        tmp_image.src = tmp;


        tmp_image.onload = function () {
            if(mode == 1 || mode == 8)
            {
                myContext.globalCompositeOperation = "source-over";
            }
            myContext.clearRect(0,0,800,600);
            myContext.drawImage(tmp_image, 0, 0); 

            if(mode == 1 || mode == 8)
            {
                myContext.globalCompositeOperation = "destination-out";
            }
        };
    }
}

function redo()
{
    if(myhistory.redo_num == 0)
    {
        console.log("no redo");
        return;
    }
    else
    {
        myhistory.undo_num++;
        myhistory.redo_num--;

        var tmp = myhistory.redo_list.pop();
        var now = myCanvas.toDataURL();
        myhistory.undo_list.push(now);

        var tmp_image = new Image();
        tmp_image.src = tmp;

        
        tmp_image.onload = function () {
            if(mode == 1 || mode == 8)
            {
                myContext.globalCompositeOperation = "source-over";
            }
            myContext.clearRect(0,0,800,600);
            myContext.drawImage(tmp_image, 0, 0); 
            if(mode == 1 || mode == 8)
            {
                myContext.globalCompositeOperation = "destination-out";
            }
        };
    }
}

function onclick_line()
{
    mode = 7;
    if(IsTextExist)
    {
        drawText();
    }
    myContext.globalCompositeOperation = "source-over";
    myCanvas_buffer.style.visibility = "visible";
    myCanvas_buffer.style.cursor = "url(./assets/SimBlack/Precision.cur), crosshair";
    myContext.strokeStyle = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';
    myContext.fillStyle = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';
    console.log("line");
}

function onclick_rangeeraser()
{
    mode = 8;

    if(IsTextExist)
    {
        drawText();
    }
    myContext.globalCompositeOperation = "destination-out";
    myCanvas_buffer.style.visibility = "visible";
    myContext_buffer.fillStyle = 'rgb(255,255,255)';

    myCanvas_buffer.style.cursor = "url(./assets/SimBlack/Precision.cur), crosshair";
}



function onchange_width()
{
    var linewidth = document.getElementById("linewidth").value/10;
    myContext.lineWidth = linewidth;
    myContext_buffer.lineWidth = linewidth;
    console.log("width change");
}

function onchange_font()
{
    myfontselec = document.getElementById("myfontselec");
    var selected = myfontselec.value;
    if(selected == 0)
    {
        myfontselec.style.fontFamily = "'Montserrat', sans-serif";
        myContext.font = text_size + "px 'Montserrat', sans-serif";
        text_font = "'Montserrat', sans-serif";
    }
    else if(selected == 1)
    {
        myfontselec.style.fontFamily = "'Mrs Sheppards', cursive";
        myContext.font = text_size + "px 'Mrs Sheppards', cursive";
        text_font = "'Mrs Sheppards', cursive";
    }
    else if(selected == 2)
    {
        myfontselec.style.fontFamily = "'Indie Flower', cursive";
        myContext.font = text_size + "px 'Indie Flower', cursive";
        text_font = "'Indie Flower', cursive";
    }
    else if(selected == 3)
    {
        myfontselec.style.fontFamily = "'Noto Sans TC', sans-serif";
        myContext.font = text_size + "px 'Noto Sans TC', sans-serif";
        text_font = "'Noto Sans TC', sans-serif";
    }
    else if(selected == 4)
    {
        myfontselec.style.fontFamily = "'Press Start 2P', cursive";
        myContext.font = text_size + "px 'Press Start 2P', cursive";
        text_font = "'Press Start 2P', cursive";
    }
    else if(selected == 5)
    {
        myfontselec.style.fontFamily = "'Dokdo', cursive";
        myContext.font = text_size + "px 'Dokdo', cursive";
        text_font = "'Dokdo', cursive";
    }

    console.log("font change");
}

function onchange_fontsize()
{
    text_size = document.getElementById("fontsize").value;
    myContext.font = text_size + "px " + text_font;
    if(IsTextExist)
    {
        var input = document.getElementById("myTextinput");
        // input.style.fontFamily = text_font;
        input.style.fontSize = text_size + "px";
    }
    console.log(text_size);
    console.log("fontsize change");
}








function getMousePos(canvas, evt)
{
    var canvas_bound = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - canvas_bound.left,
        y: evt.clientY - canvas_bound.top
    };
}

function mouseMove(evt)
{
    var mousePos = getMousePos(myCanvas, evt);
    
    myContext.lineTo(mousePos.x, mousePos.y);
    myContext.stroke();
}

function drawText()
{
    var tmp = myCanvas.toDataURL();
    myhistory.undo_num++;
    myhistory.undo_list.push(tmp);

    var input = document.getElementById('myTextinput');

    if(drawtype == 1)
    {
        myContext.fillText(
            input.value,
            text_x,
            text_y + text_size/4
        )
    }
    else
    {
        myContext.strokeText(
            input.value,
            text_x,
            text_y + text_size/4
        )
    }
    


    document.getElementById('div_canvas').removeChild(input);
    IsTextExist = false;
}



function myCanvas_mousedown(evt)
{
    var mousePos = getMousePos(myCanvas, evt);
        
    if(mode == 2)
    {
        if(IsTextExist)
        {
            drawText();
        }
        else
        {
            var input = document.createElement("input");
            input.type = "text";
            input.className = "textinput";
            input.id = "myTextinput";
            input.placeholder = "text here";

            input.style.fontFamily = text_font;
            input.style.fontSize = text_size + "px";
            input.style.color = 'rgba(' + color_now_R + ', ' + color_now_G + ', ' + color_now_B + ', ' + '1)';
            
            
            var string = evt.offsetX + myCanvas.getBoundingClientRect().left + "px";
            input.style.left = string;
            string = evt.offsetY + myCanvas.offsetTop + "px";
            input.style.top = string;

            text_x = mousePos.x;
            text_y = mousePos.y;

            document.getElementById('div_canvas').appendChild(input);

            IsTextExist = true;
        }
        
        console.log("text down");
    }
    else if(mode == 6)
    {
        if(imgupload != null)
        {
            var tmp = myCanvas.toDataURL();
            myhistory.undo_num++;
            myhistory.undo_list.push(tmp);

            myContext.drawImage(imgupload, mousePos.x, mousePos.y);
        }
        else
        {
            alert("必須先選擇圖片!");
        }
        
    }
    else if(mode == 0 || mode == 1)
    {
        var tmp = myCanvas.toDataURL();
        myhistory.undo_num++;
        myhistory.undo_list.push(tmp);

        myContext.beginPath();
        myContext.moveTo(mousePos.x, mousePos.y);
        evt.preventDefault();
        myCanvas.addEventListener('mousemove', mouseMove, false);

        console.log("Mouse down draw");
    }
    
    myhistory.redo_num = 0;
    console.log("Mouse down");
}





function myCanvasBuffer_mouseMove(evt)
{
    var mousePos = getMousePos(myCanvas_buffer, evt);

    myContext_buffer.clearRect(0,0,800,600);


    if(mode == 3)
    {
        myContext_buffer.beginPath();

        var radius = Math.sqrt(Math.pow(mousePos.x-shapefirstpoint_x, 2) 
                            + Math.pow(mousePos.y-shapefirstpoint_y, 2));

        myContext_buffer.arc(shapefirstpoint_x, shapefirstpoint_y,
            radius, 0, Math.PI*2);

        if(drawtype == 1)
        {
            myContext_buffer.fill();
            console.log("circle fill");
        }
        else
        {
            myContext_buffer.stroke();
        }
    }
    else if(mode == 4)
    {
        if(drawtype == 1)
        {
            myContext_buffer.fillRect(shapefirstpoint_x, shapefirstpoint_y
                , mousePos.x-shapefirstpoint_x, mousePos.y-shapefirstpoint_y);
        }
        else
        {
            myContext_buffer.strokeRect(shapefirstpoint_x, shapefirstpoint_y
                , mousePos.x-shapefirstpoint_x, mousePos.y-shapefirstpoint_y);
        }
        
    }
    else if(mode == 5)
    {
        myContext_buffer.beginPath();

        myContext_buffer.moveTo(shapefirstpoint_x, shapefirstpoint_y);
        myContext_buffer.lineTo(mousePos.x, mousePos.y);
        myContext_buffer.lineTo(2*shapefirstpoint_x-mousePos.x, mousePos.y);
        myContext_buffer.lineTo(shapefirstpoint_x, shapefirstpoint_y);

        if(drawtype == 1)
        {
            myContext_buffer.fill();
        }
        else
        {
            myContext_buffer.stroke();
        }
    }
    else if(mode == 7)
    {
        myContext_buffer.beginPath();

        myContext_buffer.moveTo(shapefirstpoint_x, shapefirstpoint_y);
        myContext_buffer.lineTo(mousePos.x, mousePos.y);

        myContext_buffer.stroke();
        
    }
    else if(mode == 8)
    {
        myContext_buffer.fillRect(shapefirstpoint_x, shapefirstpoint_y
            , mousePos.x-shapefirstpoint_x, mousePos.y-shapefirstpoint_y);
    }
}

function myCanvasBuffer_mousedown(evt)
{

    var mousePos = getMousePos(myCanvas_buffer, evt);
        
    // myContext_buffer.beginPath();
    // myContext_buffer.moveTo(mousePos.x, mousePos.y);
    evt.preventDefault();
    myCanvas_buffer.addEventListener('mousemove', myCanvasBuffer_mouseMove, false);

    shapefirstpoint_x = mousePos.x;
    shapefirstpoint_y = mousePos.y;

    myhistory.redo_num = 0;
    
    console.log("Mouse down 2");
}

function myCanvasBuffer_mouseup(evt)
{
    myCanvas_buffer.removeEventListener('mousemove', myCanvasBuffer_mouseMove, false);

    myContext_buffer.clearRect(0,0,800,600);

    var mousePos = getMousePos(myCanvas_buffer, evt);
        
    var tmp = myCanvas.toDataURL();



    if(mode == 3)
    {
        myContext.beginPath();
        
        var radius = Math.sqrt(Math.pow(mousePos.x-shapefirstpoint_x, 2) 
                            + Math.pow(mousePos.y-shapefirstpoint_y, 2));


        // tmp = myCanvas.toDataURL();


        myContext.arc(shapefirstpoint_x, shapefirstpoint_y, radius, 0, Math.PI*2);

        if(drawtype == 1)
        {
            myContext.fill();
            console.log("circle fill");
        }
        else
        {
            myContext.stroke();
        }
    }
    else if(mode == 4)
    {
        if(drawtype == 1)
        {
            myContext.fillRect(shapefirstpoint_x, shapefirstpoint_y
                , mousePos.x-shapefirstpoint_x, mousePos.y-shapefirstpoint_y);
        }
        else
        {
            myContext.strokeRect(shapefirstpoint_x, shapefirstpoint_y
                , mousePos.x-shapefirstpoint_x, mousePos.y-shapefirstpoint_y);
        }
    }
    else if(mode == 5)
    {
        myContext.beginPath();

        myContext.moveTo(shapefirstpoint_x, shapefirstpoint_y);
        myContext.lineTo(mousePos.x, mousePos.y);
        myContext.lineTo(2*shapefirstpoint_x-mousePos.x, mousePos.y);
        myContext.lineTo(shapefirstpoint_x, shapefirstpoint_y);

        if(drawtype == 1)
        {
            myContext.fill();
        }
        else
        {
            myContext.stroke();
        }
    }
    else if(mode == 7)
    {
        myContext.beginPath();

        myContext.moveTo(shapefirstpoint_x, shapefirstpoint_y);
        myContext.lineTo(mousePos.x, mousePos.y);

        myContext.stroke();
    }
    else if(mode == 8)
    {
        myContext.fillRect(shapefirstpoint_x, shapefirstpoint_y
            , mousePos.x-shapefirstpoint_x, mousePos.y-shapefirstpoint_y);
    }
    

    myhistory.undo_num++;
    myhistory.undo_list.push(tmp);

    console.log("Mouse up 2");
}


function picking(evt)
{
    var mousePos = getMousePos(myColorSelec_Canvas, evt);
    var pixel = myColorSelec.getImageData(mousePos.x, mousePos.y, 1, 1);
    var data = pixel.data;
    var rgba = 'rgba(' + data[0] + ', ' + data[1] +
                ', ' + data[2] + ', ' + '1)';
    myShowColor.style.background =  rgba;
    // myShowColor.textContent = rgba;
    myShowColor.textContent = 'rgb(' + data[0] + ', ' + data[1] +', ' + data[2] + ')';
    if(data[0]>=240 && data[1]>=240 && data[2]>=240)
    {
        myShowColor.style.color = 'rgb(0, 0, 0)';
    }
    else
    {
        myShowColor.style.color = 'rgb(255, 255, 255)';
    }
}

function pick(evt)
{
    var mousePos = getMousePos(myColorSelec_Canvas, evt);
    var pixel = myColorSelec.getImageData(mousePos.x, mousePos.y, 1, 1);
    var data = pixel.data;
    var rgba = 'rgba(' + data[0] + ', ' + data[1] +
                ', ' + data[2] + ', ' + '1)';
    myShowColor_now.style.background =  rgba;
    // myShowColor_now.textContent = rgba;
    myShowColor_now.textContent = 'rgb(' + data[0] + ', ' + data[1] +', ' + data[2] + ')';
    if(data[0]>=240 && data[1]>=240 && data[2]>=240)
    {
        myShowColor_now.style.color = 'rgb(0, 0, 0)';
    }
    else
    {
        myShowColor_now.style.color = 'rgb(255, 255, 255)';
    }

    myContext.strokeStyle = rgba;
    myContext.fillStyle = rgba;

    myContext_buffer.strokeStyle = rgba;
    myContext_buffer.fillStyle = rgba;

    var textinput = document.getElementById("myTextinput");
    if(textinput != null)
    {
        textinput.style.color = rgba;
    }

    color_now_R = data[0];
    color_now_G = data[1];
    color_now_B = data[2];


    console.log("ColorSelector : color picked");
}
